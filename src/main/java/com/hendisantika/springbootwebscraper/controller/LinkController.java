package com.hendisantika.springbootwebscraper.controller;

import com.hendisantika.springbootwebscraper.dto.LinkDTO;
import com.hendisantika.springbootwebscraper.entity.Link;
import com.hendisantika.springbootwebscraper.entity.Tag;
import com.hendisantika.springbootwebscraper.entity.User;
import com.hendisantika.springbootwebscraper.service.LinkService;
import com.hendisantika.springbootwebscraper.service.TagService;
import com.hendisantika.springbootwebscraper.service.UserService;
import com.hendisantika.springbootwebscraper.util.Pager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Collection;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-web-scraper
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/04/21
 * Time: 07.04
 */
@Controller
public class LinkController {
    private static final int INITIAL_PAGE = 0;

    private final TagService tagService;
    private final LinkService linkService;
    private final UserService userService;

    @Autowired
    public LinkController(TagService tagService, LinkService linkService, UserService userService) {
        this.tagService = tagService;
        this.linkService = linkService;
        this.userService = userService;
    }

    /**
     * GET handler for new link form
     * returns LinkDTO in model as a backing bean for form
     */
    @GetMapping(value = "/newLink")
    public ModelAndView newPost(Principal principal) {
        ModelAndView modelAndView = new ModelAndView();
        Optional<User> user = userService.findByUsername(principal.getName());
        if (user.isPresent()) {
            LinkDTO linkDTO = new LinkDTO();
            modelAndView.addObject("linkDTO", linkDTO);
            modelAndView.setViewName("/linkForm");
        } else {
            modelAndView.setViewName("/error");
        }
        return modelAndView;
    }

    @PostMapping(value = "/newLink")
    public ModelAndView createNewPost(@Valid LinkDTO linkDTO, BindingResult bindingResult, Principal principal) {
        ModelAndView modelAndView = new ModelAndView();
        Optional<User> user = userService.findByUsername(principal.getName());
        if (user.isPresent()) {
            if (bindingResult.hasErrors()) {
                modelAndView.setViewName("/linkForm");
            } else {
                // Get collection of tags from string from text box in form
                Collection<Tag> tags = tagService.getTagsFromString(linkDTO.getTags());

                Link link = new Link();
                link.setUrl(linkDTO.getUrl());
                link.setUser(user.get());
                link.setTags(tags);
                link = linkService.saveLink(link);
                modelAndView.setViewName("redirect:/link/" + link.getId() + "/recommendTags");
            }
        } else {
            modelAndView.setViewName("/error");
        }
        return modelAndView;
    }

    /**
     * Recommend Tags resource
     * Not possible to recommend tags if user is not logged in, or if he is now the owner of the link
     */
    @GetMapping(value = "/link/{id}/recommendTags")
    public ModelAndView recommendTagsToLink(@PathVariable Long id, Principal principal) {
        ModelAndView modelAndView = new ModelAndView();
        Optional<Link> link = linkService.findLinkForId(id);
        if (link.isPresent()) {
            //  Not possible to recommend tags if user is not logged in, or if he is now the owner of the link
            if (principal == null || !principal.getName().equals(link.get().getUser().getUsername())) {
                modelAndView.setViewName("/403");
            } else {
                // Get tags from other users
                Collection<Tag> tagsFromOtherUsers = tagService.getTagsFromOtherUsersForLink(link.get());
                // Get tags from web analysis
                Collection<Tag> tagsFromWebPageAnalysis = tagService.getTagsFromWebPageAnalysis(link.get());
                modelAndView.addObject("tagsFromOtherUsers", tagsFromOtherUsers);
                modelAndView.addObject("tagsFromWebPageAnalysis", tagsFromWebPageAnalysis);
                modelAndView.addObject("link", link.get());
                modelAndView.setViewName("/recommendTags");
            }
        } else {
            modelAndView.setViewName("/error");
        }
        return modelAndView;
    }

    /**
     * Handler for adding tags to links
     * Not possible to add tag if user is not logged in, or if he is now the owner of the link
     */
    @GetMapping(value = "/link/{linkId}/addTag/{tagString}")
    public ModelAndView addTagToLink(@PathVariable("linkId") Long linkId, @PathVariable("tagString") String tagString
            , Principal principal) {
        ModelAndView modelAndView = new ModelAndView();
        Optional<Link> link = linkService.findLinkForId(linkId);
        if (link.isPresent()) {
            // Not possible to add tag if user is not logged in, or if he is now the owner of the link
            if (principal == null || !principal.getName().equals(link.get().getUser().getUsername())) {
                modelAndView.setViewName("/403");
            } else {
                Tag tag = new Tag();
                tag.setTag(tagString);
                // If there is tag with that string already in data store, use it
                Optional<Tag> tagAlreadyExist = tagService.findByTag(tagString);
                if (tagAlreadyExist.isPresent()) tag = tagAlreadyExist.get();

                if (!link.get().getTags().contains(tag)) link.get().getTags().add(tag);
                linkService.saveLink(link.get());

                //redirect back to Recommend Tags page
                modelAndView.setViewName("redirect:/link/" + link.get().getId() + "/recommendTags");
            }
        } else {
            modelAndView.setViewName("/error");
        }
        return modelAndView;
    }

    /**
     * HTTP DELETE method
     * Deletes {@link Link} with provided id
     * Not possible to delete if user is not logged in, or if he is now the owner of the link
     */
    @DeleteMapping(value = "/link/{id}")
    public ModelAndView deleteLinkWithId(@PathVariable Long id, Principal principal) {
        ModelAndView modelAndView = new ModelAndView();
        Optional<Link> link = linkService.findLinkForId(id);
        if (link.isPresent()) {
            // Not possible to delete if user is not logged in, or if he is now the owner of the link
            if (principal == null || !principal.getName().equals(link.get().getUser().getUsername())) {
                modelAndView.setViewName("/403");
            } else {
                linkService.delete(link.get());
                modelAndView.setViewName("redirect:/home");
            }
        } else {
            modelAndView.setViewName("/error");
        }
        return modelAndView;
    }

    @GetMapping(value = "/links/{username}")
    public ModelAndView blogForUsername(@PathVariable String username,
                                        @RequestParam("page") Optional<Integer> page) {

        // Evaluate page. If requested parameter is null or less than 0 (to
        // prevent exception), return initial size. Otherwise, return value of
        // param. decreased by 1.
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        ModelAndView modelAndView = new ModelAndView();
        Optional<User> user = userService.findByUsername(username);
        if (user.isPresent()) {
            Page<Link> links = linkService.findByUserOrderedByDatePageable(user.get(), PageRequest.of(evalPage, 5));
            Pager pager = new Pager(links);

            modelAndView.addObject("links", links);
            modelAndView.addObject("pager", pager);
            modelAndView.addObject("user", user.get());
            modelAndView.setViewName("/links");

        } else {
            modelAndView.setViewName("/error");
        }
        return modelAndView;
    }

}
