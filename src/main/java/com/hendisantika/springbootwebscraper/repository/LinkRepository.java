package com.hendisantika.springbootwebscraper.repository;

import com.hendisantika.springbootwebscraper.entity.Link;
import com.hendisantika.springbootwebscraper.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-web-scraper
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/04/21
 * Time: 15.51
 */
public interface LinkRepository extends JpaRepository<Link, Long> {
    Page<Link> findByUserOrderByCreateDateDesc(User user, Pageable pageable);

    Page<Link> findAllByOrderByCreateDateDesc(Pageable pageable);

    Optional<Link> findById(Long id);

    Collection<Link> findAllByUrl(String url);
}