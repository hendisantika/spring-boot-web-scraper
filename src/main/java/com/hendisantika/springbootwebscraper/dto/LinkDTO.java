package com.hendisantika.springbootwebscraper.dto;

import com.sun.istack.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-web-scraper
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/04/21
 * Time: 07.07
 */
@Data
public class LinkDTO {
    @URL(regexp = "(http(s)?:\\/\\/.)?(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+" +
            ".~#?&//=]*)",
            message = "Please provide correct URL")
    private String url;
    @NotNull
    @Length(min = 1, message = "*Please put some tags")
    private String tags;
}
