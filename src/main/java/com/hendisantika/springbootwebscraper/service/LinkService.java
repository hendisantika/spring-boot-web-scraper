package com.hendisantika.springbootwebscraper.service;

import com.hendisantika.springbootwebscraper.entity.Link;
import com.hendisantika.springbootwebscraper.entity.User;
import com.hendisantika.springbootwebscraper.repository.LinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-web-scraper
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/04/21
 * Time: 16.02
 */
@Service
public class LinkService {
    private final LinkRepository linkRepository;

    @Autowired
    public LinkService(LinkRepository linkRepository) {
        this.linkRepository = linkRepository;
    }

    public Optional<Link> findLinkForId(Long id) {
        return linkRepository.findById(id);
    }

    public Link saveLink(Link link) {
        return linkRepository.saveAndFlush(link);
    }

    public Page<Link> findByUserOrderedByDatePageable(User user, Pageable pageable) {
        return linkRepository.findByUserOrderByCreateDateDesc(user, pageable);
    }

    public Page<Link> findAllOrderedByDatePageable(Pageable pageable) {
        return linkRepository.findAllByOrderByCreateDateDesc(pageable);
    }

    public void delete(Link link) {
        linkRepository.delete(link);
    }
}
